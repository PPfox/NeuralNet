--Neural Net Simulator--
-------by Creator-------
----------v1.0----------

--Variables--


--MetaTables--

local Neuron = {}
Neuron.__index = Neuron

local BufferIO = {}
BufferIO.__index = BufferIO

--OOP--

function BufferIO.new()
	local self = {}
	return self
end

function BufferIO.addValue(self,value)
	self[#self+1] = value
end

function BufferIO.reset(self)
	self = {}
end

function BufferIO.getAll(self)
	return self
end

function Neuron.new(inputBuffer,outputBuffer)
	local self = setmetatable({},Neuron)
	self.inputBuffer = inputBuffer
	self.outputBuffer = outputBuffer
	return self
end

--Functions--


function activation(tY,tX)
	local act = 0
	for i = 1,#tY do
		act = act + tY[i]*tX[i]
	end
	return act
end

function thresholdCheck(x,y)
	if x < y then
		return 0
	else
		return 1
	end
end

function parse(str)
	local buffer = {}
	for token in string.gmatch(str,"[%d]+") do
		buffer[#buffer+1] = tonumber(token)
	end
	return buffer
end

function main(x)
	fromInput = {}
	fromMiddle = {}
	readInput = parse(x)
	expectedOutput = {readInput[#readInput]}
	readInput[#readInput] = nil
	for i = 1,middleNeurons do
		fromInput[#fromInput+1] = neuron(readInput,0.5,"input",i)
	end
	for i = 1,outputNeurons do
		fromMiddle[#fromMiddle+1] = neuron(fromInput,0.5,"middle",i)
	end
	tError = expectedOutput[1] - fromMiddle[1]
	updateWeight(middleNeurons,"input",tError,learningRate,readInput,inputNeurons)
	updateWeight(outputNeurons,"middle",tError,learningRate,fromInput,middleNeurons)
	--print(textutils.serialize({middleNeurons,tError,learningRate,readInput,inputNeurons}))
	--print(textutils.serialize({outputNeurons,tError,learningRate,fromInput,middleNeurons}))
	print("Output")
	print(textutils.serialize(fromMiddle))
	--print(textutils.serialize(weight))
end

function fileParser()
	local d
	local l = fs.open("training","r")
	d = l.readAll()
	l.close()
	local fuck = {}
	for token in string.gmatch(d,"[^\n]+") do
		fuck[#fuck+1] = token
	end
	return fuck
end

--Code--

--shell.run("clear")
--[[
print("How many input nodes?")
inputNeurons = tonumber(read())
print("How many middle nodes?")
middleNeurons = tonumber(read())
print("How many output nodes?")
outputNeurons = tonumber(read())
--]]

while true do
	hi = BufferIO.new()
	hi:addValue("meh")
	print(textutils.serialize(hi))
	
	print("file or you(f/y)")
	choice = string.sub(read(),1,1)
	if choice == "f" then
		local inputBuffer = {}
		inputBuffer = fileParser()
		for i,v in pairs(inputBuffer) do
			print(i)
			main(v)
			if i%50 == 0 then
				sleep(0)
			end
		end
	elseif choice == "y" then
		print("Input data:")
		local x = read()
		main(x)
	end
end
